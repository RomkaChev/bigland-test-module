<?php
namespace romkachev\bigland\test\module\controllers;

use romkachev\bigland\test\module\models\PlotSearchForm;
use romkachev\bigland\test\module\Module;
use romkachev\bigland\test\module\services\PlotFetcher;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

/**
 * Class ParseController
 *
 * @package romkachev\bigland\test\module\controllers
 *
 * @property Module $module
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class ParseController extends Controller
{
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $fetcher = new PlotFetcher(['parser' => $this->module->getParser()]);
        $model    = new PlotSearchForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $plots        = $fetcher->fetch($model->cns);
            $dataProvider = new ArrayDataProvider(['allModels' => $plots, 'pagination' => false]);
        } else {
            $dataProvider = new ArrayDataProvider(['allModels' => [], 'pagination' => false]);
        }


        return $this->render('index', ['dataProvider' => $dataProvider, 'model' => $model]);
    }
}