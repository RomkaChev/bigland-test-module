<?php
namespace romkachev\bigland\test\module;

use romkachev\bigland\test\component\RussianRegistryAttributesParser;
use yii\base\BootstrapInterface;

/**
 * Class Module
 *
 * @package romkachev\bigland\test\module
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @var RussianRegistryAttributesParser
     */
    private $parser = [
        'class' => 'romkachev\bigland\test\component\RussianRegistryAttributesParser'
    ];

    /**
     * @return RussianRegistryAttributesParser
     * @throws \yii\base\InvalidConfigException
     */
    public function getParser()
    {
        if(!$this->parser instanceof RussianRegistryAttributesParser){
            $this->parser = \Yii::createObject($this->parser);
        }

        return $this->parser;
    }

    /**
     * @param RussianRegistryAttributesParser $parser
     */
    public function setParser($parser)
    {
        $this->parser = $parser;
    }

    public $controllerNamespace = 'romkachev\bigland\test\module\controllers';

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        \Yii::setAlias('@romkachev/bigland/test/module/controllers', __DIR__ . '/controllers');
        \Yii::setAlias('@romkachev/bigland/test/module/commands', __DIR__ . '/commands');

        /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'romkachev\bigland\test\module\commands';
        }
    }
}