<?php
use yii\db\Migration;

/**
 * Class m170818_122751_create_extranet__region_table
 *
 * @author Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class m170818_122751_create_plot_table extends Migration
{
    public $tablePlot = 'plot';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tablePlot, [
            'id'              => $this->primaryKey(),
            'cadastralNumber' => $this->string()->unique()->notNull(),
            'address'         => $this->string(),
            'price'           => $this->decimal(20, 4),
            'area'            => $this->decimal(20, 4),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tablePlot);
    }
}
