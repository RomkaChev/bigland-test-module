<?php
namespace romkachev\bigland\test\module\models;

use yii\db\ActiveRecord;

/**
 * Class Plot
 *
 * @package romkachev\bigland\test\module\models
 *
 * @property integer $id
 * @property string  $cadastralNumber
 * @property string  $address
 * @property float   $price
 * @property float   $area
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class Plot extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['cadastralNumber', 'string'],
            ['address', 'string'],
            ['price', 'number'],
            ['area', 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cadastralNumber' => 'Кадастровый номер',
            'address'         => 'Адрес',
            'price'           => 'Стоимость',
            'area'            => 'Площадь',
        ];
    }
}