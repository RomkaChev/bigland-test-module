<?php
namespace romkachev\bigland\test\module\models;

use yii\base\Model;

/**
 * Class PlotSearch
 *
 * @package romkachev\bigland\test\module\models
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class PlotSearchForm extends Model
{
    /**
     * @var string
     */
    public $cns;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['cns', 'string'],
            ['cns', 'required'],
        ];
    }
}