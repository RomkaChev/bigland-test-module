<?php
namespace romkachev\bigland\test\module\commands;

use romkachev\bigland\test\module\models\Plot;
use romkachev\bigland\test\module\Module;
use romkachev\bigland\test\module\services\PlotFetcher;
use yii\console\Controller;
use yii\console\widgets\Table;

/**
 * Class ParseController
 *
 * @package romkachev\bigland\test\module\commands
 *
 * @property Module $module
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class ParseController extends Controller
{
    /**
     * @param string[] $cns
     *
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex(array $cns)
    {
        $fetcher = new PlotFetcher(['parser' => $this->module->getParser()]);
        $plots   = $fetcher->fetch($cns);
        $plots   = array_map(function (Plot $plot) {
            return [
                $plot->cadastralNumber,
                $plot->address,
                $plot->price,
                $plot->area,
            ];
        }, $plots);

        echo Table::widget([
            'headers' => ['CN', 'Addr', 'Price', 'Area'],
            'rows'    => $plots,
        ]);
    }
}