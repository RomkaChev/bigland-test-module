<?php
/**
 * @var View                  $this
 * @var PlotSearchForm        $model
 * @var DataProviderInterface $dataProvider
 */

use romkachev\bigland\test\module\models\PlotSearchForm;
use yii\data\DataProviderInterface;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = 'Получение кадастровых данных';
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Получение кадастровых данных</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $form = ActiveForm::begin();
            echo $form->field($model, 'cns');
            echo Html::submitButton('Получить данные', ['class' => 'btn btn-primary']);
            ActiveForm::end();
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns'      => [
                    'cadastralNumber',
                    'address',
                    'price:currency',
                    [
                        'value'  => 'area',
                        'format' => function ($value) {
                            return number_format($value, '2', ',', ' ') . ' м<sup>2</sup>';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>


