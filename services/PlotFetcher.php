<?php
namespace romkachev\bigland\test\module\services;

use romkachev\bigland\test\component\RussianRegistryAttributesParser;
use romkachev\bigland\test\module\models\Plot;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class PlotFetcher
 *
 * @package romkachev\bigland\test\module\services
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class PlotFetcher extends Component
{
    /**
     * @var RussianRegistryAttributesParser
     */
    public $parser;

    /**
     * @param string|string[] $cns
     *
     * @return array
     */
    public function fetch($cns)
    {
        $cns    = $this->prepareInput($cns);
        $exists = Plot::find()->where(['in', 'cadastralNumber', $cns])->all();
        $newCn  = array_diff($cns, ArrayHelper::getColumn($exists, 'cadastralNumber'));

        $plots = [];
        foreach ($this->parser->parse($newCn) as $cadastralPlot) {
            $plot = new Plot([
                'cadastralNumber' => $cadastralPlot->cadastralNumber,
                'address'         => $cadastralPlot->address,
                'price'           => $cadastralPlot->price,
                'area'            => $cadastralPlot->area,
            ]);
            $plot->save();
            $plots[] = $plot;
        }

        $plots = array_merge($plots, $exists);

        return $plots;
    }

    /**
     * @param string|array $cns
     *
     * @return array
     */
    private function prepareInput($cns)
    {
        if (is_string($cns)) {
            $cns = explode(",", $cns);
        }

        $cns = array_map('trim', $cns);
        $cns = array_filter($cns);
        $cns = array_unique($cns);
        return $cns;
    }
}